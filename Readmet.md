
### Define lib
```
	/*
    Crypto handler
     */
    static RC4CryptoMySQL rc4CryptoMySQL = null;
	
```

###Init if null
```
 if (rc4CryptoMySQL == null)
    initCrypto();
```

###Init function
```
	public void initCrypto() {
        String cryptoURL = FileUtil.getApplicationProperty("crypto.URL");
        Integer oAuthID = Integer.valueOf(FileUtil.getApplicationProperty("crypto.oAuthID"));

        String dbclass = FileUtil.getApplicationProperty("crypto.db.driverClassName");
        String dburl = FileUtil.getApplicationProperty("crypto.db.url");
        String dbusername = FileUtil.getApplicationProperty("crypto.db.username");
        String dbpw = FileUtil.getApplicationProperty("crypto.db.password");
        LOG.info("CRYPTO INIT : " + dbclass + ":" + dburl + ":" + dbusername + ":" + dbpw + ":" + oAuthID + ":" + cryptoURL);
        try {
            rc4CryptoMySQL = new RC4CryptoMySQL(dbclass, dburl, dbusername, dbpw, oAuthID, cryptoURL);

        } catch (Exception e) {
            LOG.error("CRYPTO INIT ERROR : " + e.getMessage(), e);
        }


    }
```

###Usage
```
if (rc4CryptoMySQL.getRc4Crypto().isEncrypted(msisdn)) {
	msisdnType = "E";
	msisdn = "94" + rc4CryptoMySQL.getRc4Crypto().decrypt(msisdn, "API", "IBIZ");
	LOG.info("Decypt : " + originalMSISDN + " : " + msisdn);
}
```

###Config File
```
crypto.URL=https://mife.dialog.lk/apicall/rc4crypto/v1.0/
crypto.oAuthID=1

crypto.db.driverClassName=com.mysql.jdbc.Driver
crypto.db.url=jdbc:mysql://localhost:3306/oauth
crypto.db.username=root
crypto.db.password=dialog@123
```
			