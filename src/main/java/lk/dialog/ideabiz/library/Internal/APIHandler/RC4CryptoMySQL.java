package lk.dialog.ideabiz.library.Internal.APIHandler;


import lk.dialog.ideabiz.library.APICall.APICall;
import lk.ideabiz.library.APICall.DataProviders.impl.MySQLIdeabizOAuthDataProviderImpl;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Malinda_07654 on 6/15/2016.
 */
public class RC4CryptoMySQL {

    RC4Crypto rc4Crypto;

    public RC4CryptoMySQL(DataSource dataSource, Integer oAuthId, String URL) {
        MySQLIdeabizOAuthDataProviderImpl mySQLIdeabizOAuthDataProvider = new MySQLIdeabizOAuthDataProviderImpl(dataSource);
        APICall apiCall = new APICall(oAuthId, mySQLIdeabizOAuthDataProvider);
        rc4Crypto = new RC4Crypto(URL, apiCall, oAuthId);

    }

    public RC4CryptoMySQL(String classname, String url, String username, String password, Integer oAuthId, String URL) {
        MySQLIdeabizOAuthDataProviderImpl mySQLIdeabizOAuthDataProvider = new MySQLIdeabizOAuthDataProviderImpl(classname,url,username,password);
        APICall apiCall = new APICall(5000, mySQLIdeabizOAuthDataProvider);
        rc4Crypto = new RC4Crypto(URL, apiCall, oAuthId);

    }

    public RC4Crypto getRc4Crypto() {
        return rc4Crypto;
    }

    public void setRc4Crypto(RC4Crypto rc4Crypto) {
        this.rc4Crypto = rc4Crypto;
    }
}
