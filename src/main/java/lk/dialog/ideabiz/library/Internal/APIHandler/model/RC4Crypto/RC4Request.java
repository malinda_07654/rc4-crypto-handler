package lk.dialog.ideabiz.library.Internal.APIHandler.model.RC4Crypto;

/**
 * Created by Malinda_07654 on 6/10/2016.
 */
public class RC4Request {
    String msisdn;
    String channel;
    String App_name;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getApp_name() {
        return App_name;
    }

    public void setApp_name(String app_name) {
        App_name = app_name;
    }
}
