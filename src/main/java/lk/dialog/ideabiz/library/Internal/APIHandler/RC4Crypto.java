package lk.dialog.ideabiz.library.Internal.APIHandler;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lk.dialog.ideabiz.library.Internal.APIHandler.model.RC4Crypto.RC4Method;
import lk.dialog.ideabiz.library.Internal.APIHandler.model.RC4Crypto.RC4Request;
import lk.dialog.ideabiz.library.Internal.APIHandler.model.RC4Crypto.RC4Response;
import lk.dialog.ideabiz.library.APICall.APICall;
import lk.dialog.ideabiz.library.model.APICall.APICallResponse;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Malinda_07654 on 6/10/2016.
 */
public class RC4Crypto {

    public String URL;
    public APICall apiCalll;
    public Integer OAuthId;
    public Gson gson;
    public Logger logger;
    Pattern pattern;

    public String encrypt(String msisdn, String channel, String appname) throws Exception {
        msisdn = msisdn.replace("tel:+", "");
        msisdn = msisdn.replace("tel:", "");
        msisdn = msisdn.replace("tel", "");
        msisdn = msisdn.replace("+", "");
        if (msisdn.startsWith("0"))
            msisdn = msisdn.substring(1);
        if (msisdn.startsWith("7"))
            msisdn = "94" + msisdn;

        return sendRequest(msisdn, channel, appname, RC4Method.encypt);

    }

    public String decrypt(String msisdn, String channel, String appname) throws Exception {
        logger.info("DECRIPTING : " + msisdn);
        msisdn = getEncriptedString(msisdn);
        return sendRequest(msisdn, channel, appname, RC4Method.decypt);

    }

    public String sendRequest(String msisdn, String channel, String appname, RC4Method method) throws Exception {
        RC4Request rc4Request = new RC4Request();
        rc4Request.setMsisdn(msisdn);
        rc4Request.setChannel(channel);
        rc4Request.setApp_name(appname);


        //Setting headers
        Map<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        header.put("Accept", "plain/text");

        //Sending API call
        APICallResponse response = null;
        try {
            logger.info("Sending Crypt API call :" + OAuthId + ":" + URL);
            response = apiCalll.sendAuthAPICall(OAuthId, URL + method.toString(), "POST", header, gson.toJson(rc4Request), false);
            logger.info("CRYPT : " + gson.toJson(response));
            RC4Response rc4Response = gson.fromJson(response.getBody(), RC4Response.class);
            return rc4Response.getMsisdn();
        } catch (Exception e) {
            logger.error("CRYPTO ERROR : " + e.getMessage(), e);
            throw e;
        }
    }

    public boolean isEncrypted(String msisdn) {
        return (msisdn.contains("-") && msisdn.toLowerCase().startsWith("etel:"));
//
//        if(msisdn.contains("-")){
//            msisdn = getEncriptedString(msisdn);
//            return pattern.matcher(msisdn).find();
//        }
//       return false;

    }

    public String getEncriptedString(String msisdn) {
        //remove etel:9477
        return msisdn.split("-")[1];
        //return msisdn.substring(9);
    }

    /***
     * @param URL      SMS API URL
     * @param apiCalll OAuth API hanndler
     * @param OAuthId  OAuth ID
     */
    public RC4Crypto(String URL, APICall apiCalll, Integer OAuthId) {
        logger = Logger.getLogger(RC4Crypto.class);
        logger.info("Creating Crypto : " + URL + ":" + OAuthId);
        this.URL = URL;
        this.apiCalll = apiCalll;
        this.OAuthId = OAuthId;
        gson = new GsonBuilder().serializeNulls().create();
        pattern = Pattern.compile("[^0-9]");
    }

}
