package lk.dialog.ideabiz.library.Internal.APIHandler.model.RC4Crypto;

/**
 * Created by Malinda_07654 on 6/15/2016.
 */
public class RC4Response extends  RC4Request{
    String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
